package nl.alserda.erik.Student570001;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.List;

import nl.alserda.erik.Student570001.model.User;
import nl.alserda.erik.Student570001.services.AuthService;
import nl.alserda.erik.Student570001.services.NewsService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignupActivity extends AppCompatActivity implements Callback<User.Response> {

    private EditText usernameEditText;
    private EditText passwordEditText;
    private Button signupBtn;
    private Button signinLink;
    private TextView errorMessageLbl;
    private AuthService authService;
    private AuthController authController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        this.authController = AuthController.getInstance();
        this.usernameEditText = findViewById(R.id.signup_username_txt);
        this.passwordEditText = findViewById(R.id.signup_password_txt);
        this.signupBtn = findViewById(R.id.signup_btn);
        this.signinLink = findViewById(R.id.signin_link);
        this.errorMessageLbl = findViewById(R.id.signup_error_message);

        authService = RetrofitBuilder.getInstance().getRetrofitBuilder().create(AuthService.class);

        // Setup listeneres
        this.signupBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String userName = usernameEditText.getText().toString();
                String password = passwordEditText.getText().toString();
                authService.register(new User(userName, password)).enqueue(SignupActivity.this);;
            }
        });

        this.signinLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            super.onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResponse(Call<User.Response> call, Response<User.Response> response) {
        if (response.isSuccessful() && response.body() != null) {
            if(response.body().Success) {
                Intent intent = new Intent(SignupActivity.this, SigninActivity.class);
                startActivity(intent);
                authService.login(new User(usernameEditText.getText().toString(), passwordEditText.getText().toString())).enqueue(new Callback<User.Response>() {
                    @Override
                    public void onResponse(Call<User.Response> call, Response<User.Response> response) {
                        authController.setAuthToken(response.body().AuthToken);
                        Intent intent = new Intent(SignupActivity.this, MainActivity.class);
                        startActivity(intent);
                    }

                    @Override
                    public void onFailure(Call<User.Response> call, Throwable t) {

                    }
                });;
            } else {
                this.errorMessageLbl.setText(response.body().Message);
            }
        }

    }

    @Override
    public void onFailure(Call<User.Response> call, Throwable t) {
        Log.i("SignupActivity", "Er is iets fout gegaan met het aanmelden: ", t);
    }
}
