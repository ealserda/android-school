package nl.alserda.erik.Student570001.model;

import android.os.Parcel;
import android.os.Parcelable;


public class NewsItem implements Parcelable {

	public int Id;
	public String Image;
	public String Title;
	public String Summary;
	public String Url;
	public boolean IsLiked;
	public String PublishDate;
	public String[] Related;

	protected NewsItem(Parcel in) {
		Id = in.readInt();
		Image = in.readString();
		Title = in.readString();
		Summary = in.readString();
		Url = in.readString();
		IsLiked = in.readByte() != 0;
		PublishDate = in.readString();
		Related = in.createStringArray();
	}

	public static final Creator<NewsItem> CREATOR = new Creator<NewsItem>() {
		@Override
		public NewsItem createFromParcel(Parcel in) {
			return new NewsItem(in);
		}

		@Override
		public NewsItem[] newArray(int size) {
			return new NewsItem[size];
		}
	};

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeInt(Id);
		dest.writeString(Image);
		dest.writeString(Title);
		dest.writeString(Summary);
		dest.writeString(Url);
		dest.writeByte((byte) (IsLiked ? 1 : 0));
		dest.writeString(PublishDate);
		dest.writeStringArray(Related);
	}
}
