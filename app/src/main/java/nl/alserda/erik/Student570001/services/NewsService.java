package nl.alserda.erik.Student570001.services;

import java.util.List;

import nl.alserda.erik.Student570001.model.Envelope;
import nl.alserda.erik.Student570001.model.Feed;
import nl.alserda.erik.Student570001.model.NewsItem;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface NewsService {

	@GET("Articles")
	Call<Envelope<List<NewsItem>>> newsItems(@Query("feed") Integer feed);

	@GET("Articles/{id}")
	Call<Envelope<List<NewsItem>>> moreNewsItems(@Path("id") int id, @Query("count") int count, @Query("feed") Integer feed);

	@GET("Articles/liked")
	Call<Envelope<List<NewsItem>>> newsFavItems();

	@PUT("Articles/{id}/like")
	Call<ResponseBody> likeItem(@Path("id") int id);

	@DELETE("Articles/{id}/like")
	Call<ResponseBody> unlikeItem(@Path("id") int id);

	@GET("Feeds")
	Call<List<Feed>> feeds();
}
