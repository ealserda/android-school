package nl.alserda.erik.Student570001.model;

public class User {

    public String UserName;
    public String Password;

    public User (String userName, String password) {
        this.UserName = userName;
        this.Password = password;
    }

    public class Response {
        public boolean Success;
        public String Message;
        public String AuthToken;
    }
}
