package nl.alserda.erik.Student570001;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import nl.alserda.erik.Student570001.model.Envelope;
import nl.alserda.erik.Student570001.model.Feed;
import nl.alserda.erik.Student570001.model.NewsItem;
import nl.alserda.erik.Student570001.services.NewsService;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity implements Callback<Envelope<List<NewsItem>>> {
	public static SharedPreferences sharedPreferences;
	private RecyclerView recyclerView;
	private NavigationView navView;
	private NewsRecyclerViewAdapter newsRecyclerViewAdapter;

	private LinearLayoutManager layoutManager;
	private DrawerLayout drawerLayout;
	private SwipeRefreshLayout mainLayout;

	private NewsService newsService;
    private ActionBarDrawerToggle drawerToggle;
	private List<NewsItem> newsItems;
	private Integer nextId;
	private boolean isLoadingItems;
	private AuthController authController;
	private FilterState filterState;
	private Integer selectedFeedId;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		this.filterState = FilterState.ALL;
		this.sharedPreferences = getSharedPreferences("AUTH", 0);
		this.authController = AuthController.getInstance();
		this.newsService = RetrofitBuilder.getInstance().getRetrofitBuilder().create(NewsService.class);
		this.newsItems = new ArrayList<>();

		this.setupLayout();
		this.setupListeners();
		fetchNewsItems();
	}

	private void setupLayout () {
		setContentView(R.layout.activity_main);

		this.mainLayout = findViewById(R.id.main_activity_refresh_layout);
		this.mainLayout.setColorSchemeResources(R.color.colorPrimary,
				android.R.color.holo_green_dark,
				android.R.color.holo_orange_dark,
				android.R.color.holo_blue_dark);

		this.layoutManager = new LinearLayoutManager(this);
		this.newsRecyclerViewAdapter = new NewsRecyclerViewAdapter(this, this.newsItems);

		this.recyclerView = findViewById(R.id.news_item_recycler_view);
		this.recyclerView.setLayoutManager(this.layoutManager);
		this.recyclerView.setAdapter(newsRecyclerViewAdapter);

		this.drawerLayout = findViewById(R.id.main_activity_drawer_layout);
		this.drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.open, R.string.close);

		this.navView = findViewById(R.id.nav_view);

		this.drawerLayout.addDrawerListener(drawerToggle);
		this.drawerToggle.syncState();

        setupFeedMenuItems();
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		setupMenuItems();
	}

	private void setupListeners () {
		this.recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
			@Override
			public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
				if (filterState != FilterState.FAVORITE) {
					int visibleItemCount = recyclerView.getChildCount();
					int totalItemCount = layoutManager.getItemCount();
					int firstVisibleItem = layoutManager.findFirstVisibleItemPosition();
					if (firstVisibleItem + visibleItemCount >= totalItemCount - 4 && !isLoadingItems) {
						fetchNewsItems();
					}
				}
			}
		});

		this.mainLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
			@Override
			public void onRefresh() {
				reloadNewsItems();
			}
		});

		navView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
			@Override
			public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
				switch(menuItem.getItemId()) {
					case R.id.nav_login_item:
						Intent intent = new Intent(MainActivity.this, SigninActivity.class);
						startActivity(intent);
						drawerLayout.closeDrawers();
						return true;
					case R.id.nav_logout_item:
						authController.logout();
						filterState = FilterState.ALL;
						reloadNewsItems();
						setupMenuItems();
						drawerLayout.closeDrawers();
						return true;
					case R.id.nav_favorite_item:
						filterState = FilterState.FAVORITE;
						setTitle(R.string.favorite);
						reloadNewsItems();
						drawerLayout.closeDrawers();
						return true;
					case R.id.nav_all_item:
						filterState = FilterState.ALL;
						selectedFeedId = null;
						setTitle(R.string.app_name);
						reloadNewsItems();
						drawerLayout.closeDrawers();
					default:
						return false;
				}
			}
		});
	}

	private void setupFeedMenuItems () {
        newsService.feeds().enqueue(new Callback<List<Feed>>() {
			@Override
			public void onResponse(Call<List<Feed>> call, Response<List<Feed>> response) {
				if (response.isSuccessful() && response.body() != null) {
					Menu nav_menu = navView.getMenu();
					List<Feed> feeds = response.body();
					for (Feed feed : feeds) {
						MenuItem item = nav_menu.add(R.id.feed_item_group, feed.Id, Menu.NONE, feed.Name);
						item.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
							@Override
							public boolean onMenuItemClick(MenuItem item) {
								filterState = FilterState.FEED;
								setTitle(item.getTitle());
								selectedFeedId = item.getItemId();
                                reloadNewsItems();
                                drawerLayout.closeDrawers();
								return false;
							}
						});
					}
				}
			}

			@Override
			public void onFailure(Call<List<Feed>> call, Throwable t) {

			}
		});
    }

	private void reloadNewsItems () {
		newsItems.clear();
		nextId = null;
		fetchNewsItems();
	}

	private void setupMenuItems () {
		Menu nav_menu = navView.getMenu();
		if (authController.isAuthenticated()) {
			nav_menu.findItem(R.id.nav_login_item).setVisible(false);
			nav_menu.findItem(R.id.nav_logout_item).setVisible(true);
			nav_menu.findItem(R.id.nav_favorite_item).setVisible(true);
		} else {
			nav_menu.findItem(R.id.nav_login_item).setVisible(true);
			nav_menu.findItem(R.id.nav_logout_item).setVisible(false);
			nav_menu.findItem(R.id.nav_favorite_item).setVisible(false);
		}
	}

	private void fetchNewsItems () {
		toggleLoadingItems(true);
		if(filterState == FilterState.FAVORITE) {
			if (nextId == null) {
				newsService.newsFavItems().enqueue(this);
			}
		} else {
			if (nextId == null) {
				newsService.newsItems(selectedFeedId).enqueue(this);
			} else {
				newsService.moreNewsItems(MainActivity.this.nextId, 20, selectedFeedId).enqueue(MainActivity.this);
			}
		}
	}

	private void toggleLoadingItems(boolean loading) {
		isLoadingItems = loading;
		mainLayout.setRefreshing(isLoadingItems);
	}

	/* ------------ OVERRIDES ------------ */
	@Override
	public boolean onOptionsItemSelected (MenuItem item) {
		if (drawerToggle.onOptionsItemSelected(item)) {
			return true;
		}

		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onResponse(Call<Envelope<List<NewsItem>>> call, Response<Envelope<List<NewsItem>>> response) {
		if (response.isSuccessful() && response.body() != null) {
			newsItems.addAll(response.body().Results);
			nextId = response.body().NextId;
			newsRecyclerViewAdapter.notifyDataSetChanged();
			toggleLoadingItems(false);
		} else {
			if(response.code() == 401) {
				authController.logout();
				filterState = FilterState.ALL;
				reloadNewsItems();
				setupMenuItems();
			} else {
				onReponseFailure();
			}
		}
	}

	@Override
	public void onFailure(Call<Envelope<List<NewsItem>>> call, Throwable t) {
		onReponseFailure();
	}

	private void onReponseFailure () {
		toggleLoadingItems(false);
		Snackbar.make(mainLayout, R.string.failure, Snackbar.LENGTH_INDEFINITE).setAction(
				R.string.retry, new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						fetchNewsItems();
					}
				}).show();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if(resultCode == RESULT_OK) {
			NewsItem newsItem = data.getParcelableExtra("result");
			for(NewsItem item : newsItems) {
				if(item.Id == newsItem.Id) {
					item.IsLiked = newsItem.IsLiked;
					newsRecyclerViewAdapter.notifyDataSetChanged();
				}
			}
		}
	}
}
