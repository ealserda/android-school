package nl.alserda.erik.Student570001;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitBuilder {
    private static RetrofitBuilder instance;
    private Retrofit retrofitBuilder;

    protected RetrofitBuilder() {
        OkHttpClient okClient = new OkHttpClient.Builder()
            .addInterceptor(new ServiceInterceptor())
            .build();
        retrofitBuilder = new Retrofit.Builder()
                .baseUrl("http://inhollandbackend.azurewebsites.net/api/")
                .client(okClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

    }
    public static RetrofitBuilder getInstance() {
        if(instance == null) {
            instance = new RetrofitBuilder();
        }
        return instance;
    }

    public Retrofit getRetrofitBuilder (){
        return this.retrofitBuilder;
    }

    class ServiceInterceptor implements Interceptor {

        @Override
        public Response intercept(Chain chain)throws IOException {
            Request request = chain.request();

            String authToken = MainActivity.sharedPreferences.getString("authToken", null);
            if (request.header("No-Authentication") == null && authToken != null) {
                request = request.newBuilder()
                        .addHeader("x-authtoken", authToken)
                        .build();
            }
            return chain.proceed(request);
        }
    }
}
