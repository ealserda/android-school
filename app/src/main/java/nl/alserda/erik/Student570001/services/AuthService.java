package nl.alserda.erik.Student570001.services;

import nl.alserda.erik.Student570001.model.User;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface AuthService {

    @POST("Users/register")
    @Headers("No-Authentication: true")
    Call<User.Response> register(@Body User user);

    @POST("Users/login")
    @Headers("No-Authentication: true")
    Call<User.Response> login(@Body User user);
}
