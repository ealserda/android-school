package nl.alserda.erik.Student570001;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import nl.alserda.erik.Student570001.model.User;
import nl.alserda.erik.Student570001.services.AuthService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SigninActivity extends AppCompatActivity implements Callback<User.Response> {

    private Button signupButton;
    private Button signinButton;
    private EditText usernameEditText;
    private EditText passwordEditText;
    private AuthService authService;
    private AuthController authController;
    private TextView errorMessageLbl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signin);

        this.authController = AuthController.getInstance();
        this.signupButton = findViewById(R.id.signup_link);
        this.signinButton = findViewById(R.id.signin_btn);
        this.usernameEditText = findViewById(R.id.username_txt);
        this.passwordEditText = findViewById(R.id.password_txt);
        this.errorMessageLbl = findViewById(R.id.signin_error_message);

        authService = RetrofitBuilder.getInstance().getRetrofitBuilder().create(AuthService.class);

        this.signupButton.setOnClickListener(new Button.OnClickListener(){

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SigninActivity.this, SignupActivity.class);
                startActivity(intent);
            }
        });

        this.signinButton.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View v) {
                String userName = usernameEditText.getText().toString();
                String password = passwordEditText.getText().toString();
                authService.login(new User(userName, password)).enqueue(SigninActivity.this);;
            }
        });
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            super.onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResponse(Call<User.Response> call, Response<User.Response> response) {
        if (response.isSuccessful() && response.body() != null) {
            authController.setAuthToken(response.body().AuthToken);
            Intent intent = new Intent(SigninActivity.this, MainActivity.class);
            startActivity(intent);
        } else {
            this.errorMessageLbl.setText(R.string.signin_error_text);
            Log.i("SignupActivity", "TODO: display errormessage");
        }
    }

    @Override
    public void onFailure(Call<User.Response> call, Throwable t) {

    }
}
