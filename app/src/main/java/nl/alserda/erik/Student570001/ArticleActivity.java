package nl.alserda.erik.Student570001;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.text.DateFormat;
import java.text.ParseException;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Locale;
import java.text.SimpleDateFormat;

import nl.alserda.erik.Student570001.model.NewsItem;
import nl.alserda.erik.Student570001.services.NewsService;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ArticleActivity extends AppCompatActivity {
    public static final String CONTENT = "nl.alserda.erik.Student570001.CONTENT";
    public static final String VIEW_NAME_IMAGE = "article:image";

    public ImageView articleImage;
    public TextView articleTitle;
    public TextView articleSummary;
    public TextView relatedArticles;
    public TextView articleDate;
    public Button readMoreButton;
    public FloatingActionButton shareButton;
    public FloatingActionButton likeButton;
    public NewsItem article;
    public NewsService newsService;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_article);

        this.articleImage = findViewById(R.id.article_image);
        this.articleImage.setTransitionName(VIEW_NAME_IMAGE);
        this.articleTitle = findViewById(R.id.article_title);
        this.articleSummary = findViewById(R.id.article_summary);
        this.articleDate = findViewById(R.id.article_date);
        this.relatedArticles = findViewById(R.id.related_articles);
        this.readMoreButton = findViewById(R.id.read_more_button);
        this.likeButton = findViewById(R.id.article_like_btn);
        this.shareButton = findViewById(R.id.article_share_btn);
        this.article = getIntent().getParcelableExtra(CONTENT);

        newsService = RetrofitBuilder.getInstance().getRetrofitBuilder().create(NewsService.class);

        this.setupListeners();
        this.setInfo();
    }

    private void setupListeners () {
        this.readMoreButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri uri = Uri.parse(article.Url);
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
            }
        });

        this.shareButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/plain");
                intent.putExtra(Intent.EXTRA_SUBJECT, R.string.share_url);
                intent.putExtra(Intent.EXTRA_TEXT, article.Url);
                startActivity(Intent.createChooser(intent, "Share url"));
            }
        });

        if(AuthController.getInstance().isAuthenticated()) {
                this.likeButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(article.IsLiked) {
                            newsService.unlikeItem(article.Id).enqueue(new Callback<ResponseBody>() {
                                @Override
                                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                    setArticleIsLiked(false);
                                    Intent resultIntent = new Intent();
                                    resultIntent.putExtra("result", article);
                                    setResult(RESULT_OK, resultIntent);
                                }

                                @Override
                                public void onFailure(Call<ResponseBody> call, Throwable t) {

                                }
                            });
                        } else {
                            newsService.likeItem(article.Id).enqueue(new Callback<ResponseBody>() {
                                @Override
                                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                    setArticleIsLiked(true);
                                    Intent resultIntent = new Intent();
                                    resultIntent.putExtra("result", article);
                                    setResult(RESULT_OK, resultIntent);
                                }

                                @Override
                                public void onFailure(Call<ResponseBody> call, Throwable t) {

                                }
                            });
                        }
                    }
                });
        }

    }

    private void setInfo () {
        try {
            DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            Date publishDate = dateFormatter.parse(article.PublishDate);
            SimpleDateFormat formatter = new SimpleDateFormat("EEEE d MMMM yyyy HH:mm");
            articleDate.setText(formatter.format(publishDate));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        articleTitle.setText(article.Title);
        articleSummary.setText(article.Summary);
        if(article.Related != null && article.Related.length > 0) {
            (findViewById(R.id.related_articles_title)).setVisibility(View.VISIBLE);
            for (String relatedLink : article.Related) {
                this.relatedArticles.append(relatedLink + "\n\n");
            }
        }

        if(AuthController.getInstance().isAuthenticated()) {
            likeButton.show();
            setLikeButton();
        } else {
            likeButton.hide();
        }
        Glide.with(this)
                .asBitmap()
                .load(article.Image)
                .into(articleImage);
    }

    private void setLikeButton () {
        if(article.IsLiked) {
            likeButton.setImageResource(R.mipmap.heartred);
        } else {
            likeButton.setImageResource(R.mipmap.heartwhite);
        }
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            super.onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void setArticleIsLiked (boolean value) {
        article.IsLiked = value;
        setLikeButton();
    }
}
