package nl.alserda.erik.Student570001;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

import nl.alserda.erik.Student570001.model.NewsItem;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;

public class NewsRecyclerViewAdapter extends RecyclerView.Adapter<NewsRecyclerViewAdapter.ViewHolder> {

	private List<NewsItem> newsItems;
	private Context context;
	private AuthController authController;

	public NewsRecyclerViewAdapter(Context context, List<NewsItem> newsItems) {
		this.newsItems = newsItems;
		this.context = context;
		this.authController = AuthController.getInstance();
	}

	@NonNull
	@Override
	public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
		View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.news_item, parent, false);
		ViewHolder holder = new ViewHolder(view);
		return holder;
	}

	@Override
	public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final int position) {
		NewsItem newsItem = newsItems.get(position);
		RequestOptions options = new RequestOptions()
				.centerCrop();

		Glide.with(this.context).load(newsItem.Image).apply(options).transition(withCrossFade()).into(viewHolder.newsItemImage);
		viewHolder.newsItemTitle.setText(newsItem.Title);

		if(authController.isAuthenticated()) {
			viewHolder.starItemImage.setVisibility(View.VISIBLE);
			if (newsItems.get(position).IsLiked) {
				viewHolder.starItemImage.setImageResource(R.mipmap.heartred);
			} else {
				viewHolder.starItemImage.setImageResource(R.mipmap.heartgray);
			}

		} else {
			viewHolder.starItemImage.setVisibility(View.GONE);
		}

		viewHolder.newsItemLayout.setOnClickListener(new View.OnClickListener(){
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(context, ArticleActivity.class);
				intent.putExtra(ArticleActivity.CONTENT, newsItems.get(position));
				ActivityOptions activityOptions = ActivityOptions.makeSceneTransitionAnimation(
						(Activity) context,
						new Pair<>(v.findViewById(R.id.news_item_image),
								ArticleActivity.VIEW_NAME_IMAGE));
				((Activity) context).startActivityForResult(intent, 1, activityOptions.toBundle());
			}
		});
	}

	@Override
	public int getItemCount() {
		return newsItems.size();
	}

	public class ViewHolder extends RecyclerView.ViewHolder {

		ImageView newsItemImage;
		ImageView starItemImage;
		TextView newsItemTitle;
		RelativeLayout newsItemLayout;

		public ViewHolder(@NonNull View itemView) {
			super(itemView);
			newsItemImage = itemView.findViewById(R.id.news_item_image);
			newsItemTitle = itemView.findViewById(R.id.news_item_title);
			newsItemLayout = itemView.findViewById(R.id.news_item);
			starItemImage = itemView.findViewById(R.id.star_item_image);
		}
	}
}
