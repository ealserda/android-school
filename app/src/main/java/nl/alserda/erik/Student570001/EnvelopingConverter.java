package nl.alserda.erik.Student570001;

import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

import nl.alserda.erik.Student570001.model.Envelope;
import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.Retrofit;

public class EnvelopingConverter extends Converter.Factory {

	@Override
	public Converter<ResponseBody, ?> responseBodyConverter(Type type, Annotation[] annotations, Retrofit retrofit) {
		final Type envalopedType = TypeToken.getParameterized(Envelope.class, type).getType();

		final Converter<ResponseBody, Envelope<?>> delegate = retrofit.nextResponseBodyConverter(this, envalopedType, annotations);


		return new Converter<ResponseBody, Object>() {
			@Override
			public Object convert(ResponseBody body) throws IOException {
				Envelope<?> envelope = delegate.convert(body);
				return envelope.Results;
			}
		};
	}
}
