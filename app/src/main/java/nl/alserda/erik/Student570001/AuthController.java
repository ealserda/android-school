package nl.alserda.erik.Student570001;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import java.util.Observable;

import static nl.alserda.erik.Student570001.MainActivity.sharedPreferences;

// Controlling the auth token, can be observed to get notified whenever the user signsin/out
public class AuthController extends Observable {
    private static AuthController instance;

    protected AuthController () {
    }

    public static AuthController getInstance() {
        if(instance == null) {
            instance = new AuthController();
        }
        return instance;
    }

    public boolean isAuthenticated (){
        return getAuthToken() != null;
    }

    public String getAuthToken() {
        return sharedPreferences.getString("authToken", null);
    }

    public void setAuthToken(String authToken) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        if (authToken == null) {
            editor.clear();
        } else {
            editor.putString("authToken", authToken);
        }
        editor.commit();
        setChanged();
        notifyObservers();
    }

    synchronized public void logout () {
        setAuthToken(null);
    }
}
